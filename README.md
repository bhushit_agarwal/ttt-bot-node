# TTT Bot#

A naive but passionate slack chatbot that plays tic-tac-toe with you using fun emoji.  

![bot-demo.gif](https://bitbucket.org/repo/z8jAzq9/images/2895908348-bot-demo.gif)

Note:

*  Bot yet cant evaluate win, lose or draw. Need evaluation logic.
*  Sensitive to user input. 
*  Plays only random moves.

### Setup ###
```
npm install
SLACK_TOKEN=<YOUR_SLACK_TOKEN> npm start
```
You can get your slack token [here](https://my.slack.com/services/new/bot).