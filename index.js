var Botkit = require('botkit')

var token = process.env.SLACK_TOKEN

var controller = Botkit.slackbot({
  // reconnect to Slack RTM when connection goes bad
  retry: Infinity,
  debug: false
})

// Connect to Slack RTM
controller.spawn({
  token: token
}).startRTM(function (err, bot, payload) {
  if (err) {
    throw new Error(err)
  }
  console.log('Connected to Slack RTM')
})

// Say "I'm here" when we're invited to join a channel
controller.on('bot_channel_join', function (bot, message) {
  bot.reply(message, "I'm here!")
})

// Say "Hello" when we hear "hi"
controller.hears(['hi'], ['ambient', 'direct_message','direct_mention','mention'], function (bot, message) {
  const { user, channel, text } = message; // destructure message variable
  bot_name = bot.identity.name;
  bot.reply(message, `Hello. I am <@${bot_name}>` )
})

// Start a new game when hears play, new game
controller.hears(['play','new game'], ['ambient', 'direct_message','direct_mention','mention'], function (bot, message) {
  const { user, channel, text } = message; // destructure message variable
  bot_name = bot.identity.name;
  initBoard = function() {
    var size = 3;
    var availableMoves = new Map();
    for(var i=0; i < size*size; i++) {
      availableMoves.set(i,true);
    }
    //gameData.game.availableMoves = availableMoves;
    console.log("Initialized availableMoves. Its value now is");
    console.log(JSON.stringify(gameData.game.availableMoves));
    return availableMoves;
  }

  const gameData = {
    id: channel,
    game: {
      board: [[-1,-1,-1],[-1,-1,-1],[-1,-1,-1]],
      boardSize: 3,
      availableMoves: {
        0: true,
        1: true,
        2: true,
        3: true,
        4: true,
        5: true,
        6: true,
        7: true,
        8: true,
      },
    },
    players: {
      [user]: {
        sym: '',
        played: '',
      },
      [bot_name]: {
        sym: ':robot_face:',
        played: '',
      },
    },
  };


  askSym = function(response, convo) {
      convo.ask(`Lets play <@${user}>. I am going to use ` + gameData.players[bot_name].sym + ` as my marker for the game. What would you like to use?`, function(response, convo) {
        initBoard();
        storeSym(response.text)
        convo.say('Sweeet! I have saved '+gameData.players[user].sym + ' as your marker!');
        convo.say(`Lets begin.`);
        convo.say(`Board will look like ` + printBoard(gameData.game.board));
        askPlayerMove(response,convo);
        convo.next();
      });
  }

  askPlayerMove = function(response, convo) {
      convo.ask(`Its your turn <@${user}>`, function(response, convo) {
      board = gameData.game.board;
      move = response.text;
      r = Math.floor(move/board.length);
      c = move%board.length;
      while(!isValidMove(move)) {
        console.log('Invalid move'+JSON.stringify(gameData.game.availableMoves));
        convo.say('Invalid move. Available moves are '+gameData.game.availableMoves );
        convo.next();
        convo.repeat();
        convo.next();
      }
      
      board[r][c] = gameData.players[user].sym;
      gameData.game.board = board;
      gameData.game.availableMoves[move] = false;
      var cmove = computerMove(gameData.game.board);
      convo.say(`Nice! I play `+cmove);
      convo.say(`Game now looks like `+printBoard(gameData.game.board));
      if(areMovesAvailable()) {
        convo.repeat();
      } else {
        convo.say('Looks like the game is over!');
      }
      convo.next();
    });
  }

  askBoardSize = function(response, convo) {
    convo.ask(`What should be the grid size for the game?`, function(response, convo) {
      convo.say(`:thumbsup:`);
    });
  }

  areMovesAvailable = function() {
    moves = gameData.game.availableMoves;
    for(var i=0; i<Object.keys(moves).length; i++) {
      if(moves[i])
        return true;
    }
    return false;
  }

  computerMove = function(board) {
    boardSize = gameData.game.boardSize;
    do {
      move = Math.floor(Math.random() * ((boardSize * boardSize) -1))
      r = Math.floor(move/boardSize);
      c = move%boardSize;
    } while(!isValidMove(move));
    board[r][c] = gameData.players[bot_name].sym;
    gameData.game.board = board;
    gameData.game.availableMoves[move] = false;
    return move;
  }

  isValidMove = function (move) {
    return gameData.game.availableMoves[move];
  }

  storeSym = function(sym) {
    gameData.players[user].sym = sym;
  }

  printBoard = function(board) {
    var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
    var boardStr = '\n';
    var size = board.length;
    for(var i=0; i< size; i++) {
      for(var j=0; j < size; j++) {
        if(board[i][j] === -1) {
          boardStr += ':' + dg[i*size+j] + ':';
        } else {
          boardStr += board[i][j];
        }
        if(j != size - 1){
          boardStr += '  ';
        }
      }
      boardStr += '\n';
    }
    boardStr += '\n';
    return boardStr;
  }

  bot.startConversation(message, askSym);
})
